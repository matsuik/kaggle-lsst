import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.metrics import log_loss
from sklearn.model_selection import StratifiedKFold
import gc
import os
import matplotlib.pyplot as plt
import seaborn as sns
import lightgbm as lgb
import itertools
import pickle, gzip
import glob
import importlib
import time

from joblib import dump, load

from cv_sampler import *

np.warnings.filterwarnings('ignore')


def submit_scv(gal_clf, ex_clf, gal_df, ex_df):

    pred_proba = gal_clf.predict_proba(gal_df)

    proba_df = pd.DataFrame(columns=[
        'object_id', 'class_6', 'class_15', 'class_16', 'class_42', 'class_52',
        'class_53', 'class_62', 'class_64', 'class_65', 'class_67', 'class_88',
        'class_90', 'class_92', 'class_95', 'class_99'
    ])

    proba_df['object_id'] = gal_df.index
    classes = [6, 16, 53, 65, 92]
    for i in range(len(classes)):
        proba_df[f'class_{classes[i]}'] = pred_proba[:, i]
    proba_df.fillna(0, inplace=True)

    pred_proba = ex_clf.predict_proba(ex_df)

    ex_proba_df = pd.DataFrame(columns=[
        'object_id', 'class_6', 'class_15', 'class_16', 'class_42', 'class_52',
        'class_53', 'class_62', 'class_64', 'class_65', 'class_67', 'class_88',
        'class_90', 'class_92', 'class_95', 'class_99'
    ])
    ex_proba_df['object_id'] = ex_df.index
    classes = [15, 42, 52, 62, 64, 67, 88, 90, 95]
    for i in range(len(classes)):
        ex_proba_df[f'class_{classes[i]}'] = pred_proba[:, i]
    ex_proba_df.fillna(0, inplace=True)

    return pd.concat([proba_df, ex_proba_df], ignore_index=True)


def write_submit_unknown_onehot(subdf, filename):
    subdf.to_csv(
        os.path.join('../results', filename + '_raw_proba.csv'), index=False)

    subdf.to_csv(
        os.path.join('../results', filename + '_raw_proba.csv'), index=False)

    onehot_df = pd.DataFrame(0, index=subdf.index, columns=subdf.columns)
    for i, key in enumerate(subdf.drop(columns=['object_id']).idxmax(axis=1)):
        onehot_df.loc[i, key] = 1
    onehot_df.object_id = subdf.object_id

    onehot_df.to_csv(
        os.path.join('../results', filename + '_raw_onehot.csv'), index=False)

    unknown_df = subdf.copy()
    for i, max_ in enumerate(subdf.drop(columns=['object_id']).max(axis=1)):
        if max_ < 0.4:
            unknown_df.loc[i, 'class_99'] = 1 - max_

    onehot_df = pd.DataFrame(0, index=subdf.index, columns=subdf.columns)
    for i, key in enumerate(
            unknown_df.drop(columns=['object_id']).idxmax(axis=1)):
        onehot_df.loc[i, key] = 1
    onehot_df.object_id = subdf.object_id

    unknown_df.to_csv(
        os.path.join('../results', filename + '_unknown.csv'), index=False)
    onehot_df.to_csv(
        os.path.join('../results', filename + '_unknown_onehot.csv'),
        index=False)
