import numpy as np


class UniformSampler:
    def __init__(self, start, end, dtype=float):
        self.start = start
        self.end = end
        self.dtype = dtype

    def rvs(self, random_state=None):
        return self.dtype(np.random.uniform(self.start, self.end))


class LogUniformSampler:
    def __init__(self, start, end, dtype=float):
        self.start = start
        self.end = end
        self.dtype = dtype

    def rvs(self, random_state=None):
        return self.dtype(10**np.random.uniform(self.start, self.end))
